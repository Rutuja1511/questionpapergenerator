import sqlite3
import random
import fpdf

from tkinter import *

sql = sqlite3.connect("question.db")
cur = sql.cursor()
pdf = fpdf.FPDF(format='A4')


def recordentry(Ques,diff):
    sql_id_cmd = ("SELECT MAX(ID) FROM "+diff)
    cur.execute(sql_id_cmd)
    data = cur.fetchone()
    if data[0] is None:
        i = 1
    else:
        i = data[0] + 1
    sql_in_cmd = ("INSERT INTO "+diff+" VALUES("+str(i)+",\""+Ques+"\");")
    print(sql_in_cmd)
    cur.execute(sql_in_cmd)
    sql.commit()

def questionselector(diff):
    diff = diff.upper()
    print(diff)

    sql_id_cmd1 = ("SELECT MAX(ID) FROM "+diff)

    cur.execute(sql_id_cmd1)
    data1=cur.fetchone()
    

    if data1[0] is None:
        print("one or more tables are Empty")
        exit()
    elif data1[0]<10:
        print("Not sufficient elements in Tables")
        exit()
    else:
        i1 = data1[0]
        rand1 = random_num_gen(i1)
        print(rand1)

    j=0
    obj1 = []

    for i in range(10):
        sql_in_cmd1 = ("SELECT Ques FROM "+diff+" WHERE "+"ID = "+str(rand1[j]))
       
        j = j+1
        cur.execute(sql_in_cmd1)
        obj1.append(list(cur.fetchone()))
        
    print(obj1)
    pdf_gen(obj1)

def pdf_gen(list1):
    pdf.add_page()
    pdf.set_font("Times", size=14)

    pdf.cell(200,15,"Practice Question Paper", ln=1, align="C")
    pdf.set_font("Times",'i', size=17)
    pdf.cell(200,15,"  ", ln=1, align="C")
    pdf.set_font("Times",'i', size=14)
    pdf.set_font("Times", size=12)

    for i in range(10):
        pdf.cell(170,6,"Q"+str(i+1)+": "+list1[i][0],ln=1,align="left")

    pdf.set_font("Arial",'b', size=16)

    
    pdf.output("file.pdf")

def random_num_gen(n):
    rlist = random.sample(range(n),10)
    rlist = [x+1 for x in rlist]
    return rlist

def mainwin():
    window = Tk()
    window.title("Question Paper Generator")
    window.configure(background='#ECECEC')
    window.geometry('800x500')

    lbl = Label(window, font="SF\Mono 36 bold", text = "Question Paper\nGenerator",background='#ECECEC',justify='left')
    lbl.grid(column=0, row=0,padx=20,pady=10)

    frame=Frame(window)
    frame.grid(column=0,row=3,padx=0,pady=10)

    addbutton=Button(frame,text="Add Question",)
    addbutton.config(height = 2, width = 15,bg='#ECECEC',justify='left',bd='0',relief='raised',command=addQwin)
    addbutton.grid(column=0,row=2)

    genbutton=Button(frame,text="Generate PDF",)
    genbutton.config(height = 2, width = 15,bg='#ECECEC',justify='left',bd='0',relief='raised',command=genpdf)
    genbutton.grid(column=1,row=2)

    window.mainloop()

def addQwin():
    window = Tk()
    window.title("Add Questions")
    window.configure(background='#ECECEC')
    window.geometry('460x460')

    txt2 = StringVar(window)
    txt3 = StringVar(window)
    txt3.set("English")

    def clicked():
        Ques = txt1.get()
        marks = txt2.get()
        diff = txt3.get()
        print(Ques,diff)
        recordentry(Ques,diff)

    lbl1 = Label(window, font="SF\Mono 36 bold", text = "Add\nQuestions",background='#ECECEC',justify='left')
    lbl1.grid(column=0, row=0,padx=20,pady=10)
    frame1=Frame(window)
    frame1.grid(column=0,row=2,padx=0,pady=10)
    txt1 = Entry(frame1,width=20,bg='#ECECEC')
    txt1.grid(column=0, row=0)

    lbl3 = Label(window, font="SF\Mono 26 bold", text = "Enter subject",background='#ECECEC',justify='left')
    lbl3.grid(column=0, row=5,padx=20,pady=10)
    frame3=Frame(window)
    frame3.grid(column=0,row=6,padx=0,pady=10)
    optionm = OptionMenu(frame3,txt3,"English","Quant","Reasoning")
    optionm.grid(column=0, row=0)

    frame4=Frame(window)
    frame4.grid(column=0,row=7,padx=0,pady=10)
    btn = Button(frame4, text="Submit",command=clicked)
    btn.grid(column=0, row=0)

def genpdf():
    window = Tk()
    window.title("Generate PDF")
    window.configure(background='#ECECEC')

    def clickenglish():
        questionselector("English")
        lbl2 = Label(window, font="SF\Mono 14 bold", text = "PDF Generated in\ncurrent working directory",background='#ECECEC')
        lbl2.grid(column=0, row=3,padx=10,pady=5)

    def clickquant():
        questionselector("Quant")
        lbl2 = Label(window, font="SF\Mono 14 bold", text = "PDF Generated in\ncurrent working directory",background='#ECECEC')
        lbl2.grid(column=0, row=3,padx=20,pady=10)

    def clickreasoning():
        questionselector("Reasoning")
        lbl2 = Label(window, font="SF\Mono 14 bold", text = "PDF Generated in\ncurrent working directory",background='#ECECEC')
        lbl2.grid(column=0, row=3,padx=20,pady=10)

    lbl = Label(window, font="SF\Mono 36 bold", text = "Generate\nPDF",background='#ECECEC',justify='left')
    lbl.grid(column=0, row=0,padx=20,pady=10)
    frame=Frame(window,background='#ECECEC')
    frame.grid(column=0,row=2,padx=0,pady=10)
    btn1 = Button(frame, text="English",command=clickenglish)
    btn1.config(height = 2)
    btn1.grid(column=0, row=0)
    btn2 = Button(frame, text="Quant",command=clickquant)
    btn2.config(height = 2)
    btn2.grid(column=1, row=0)
    btn3 = Button(frame, text="Reasoning",command=clickreasoning)
    btn3.config(height = 2,background='#ECECEC')
    btn3.grid(column=2, row=0)


mainwin()
sql.close()
